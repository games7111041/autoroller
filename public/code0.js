gdjs.TitleCode = {};
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects1= [];
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects2= [];
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects3= [];
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects4= [];
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects5= [];
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects6= [];
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects7= [];
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects8= [];
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects9= [];
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects10= [];
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects11= [];
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects12= [];
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects13= [];
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects14= [];
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects15= [];
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects16= [];
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects17= [];
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects18= [];
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects19= [];
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects20= [];
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects21= [];
gdjs.TitleCode.GDhelpObjects1= [];
gdjs.TitleCode.GDhelpObjects2= [];
gdjs.TitleCode.GDhelpObjects3= [];
gdjs.TitleCode.GDhelpObjects4= [];
gdjs.TitleCode.GDhelpObjects5= [];
gdjs.TitleCode.GDhelpObjects6= [];
gdjs.TitleCode.GDhelpObjects7= [];
gdjs.TitleCode.GDhelpObjects8= [];
gdjs.TitleCode.GDhelpObjects9= [];
gdjs.TitleCode.GDhelpObjects10= [];
gdjs.TitleCode.GDhelpObjects11= [];
gdjs.TitleCode.GDhelpObjects12= [];
gdjs.TitleCode.GDhelpObjects13= [];
gdjs.TitleCode.GDhelpObjects14= [];
gdjs.TitleCode.GDhelpObjects15= [];
gdjs.TitleCode.GDhelpObjects16= [];
gdjs.TitleCode.GDhelpObjects17= [];
gdjs.TitleCode.GDhelpObjects18= [];
gdjs.TitleCode.GDhelpObjects19= [];
gdjs.TitleCode.GDhelpObjects20= [];
gdjs.TitleCode.GDhelpObjects21= [];
gdjs.TitleCode.GDtitleObjects1= [];
gdjs.TitleCode.GDtitleObjects2= [];
gdjs.TitleCode.GDtitleObjects3= [];
gdjs.TitleCode.GDtitleObjects4= [];
gdjs.TitleCode.GDtitleObjects5= [];
gdjs.TitleCode.GDtitleObjects6= [];
gdjs.TitleCode.GDtitleObjects7= [];
gdjs.TitleCode.GDtitleObjects8= [];
gdjs.TitleCode.GDtitleObjects9= [];
gdjs.TitleCode.GDtitleObjects10= [];
gdjs.TitleCode.GDtitleObjects11= [];
gdjs.TitleCode.GDtitleObjects12= [];
gdjs.TitleCode.GDtitleObjects13= [];
gdjs.TitleCode.GDtitleObjects14= [];
gdjs.TitleCode.GDtitleObjects15= [];
gdjs.TitleCode.GDtitleObjects16= [];
gdjs.TitleCode.GDtitleObjects17= [];
gdjs.TitleCode.GDtitleObjects18= [];
gdjs.TitleCode.GDtitleObjects19= [];
gdjs.TitleCode.GDtitleObjects20= [];
gdjs.TitleCode.GDtitleObjects21= [];
gdjs.TitleCode.GDHowToObjects1= [];
gdjs.TitleCode.GDHowToObjects2= [];
gdjs.TitleCode.GDHowToObjects3= [];
gdjs.TitleCode.GDHowToObjects4= [];
gdjs.TitleCode.GDHowToObjects5= [];
gdjs.TitleCode.GDHowToObjects6= [];
gdjs.TitleCode.GDHowToObjects7= [];
gdjs.TitleCode.GDHowToObjects8= [];
gdjs.TitleCode.GDHowToObjects9= [];
gdjs.TitleCode.GDHowToObjects10= [];
gdjs.TitleCode.GDHowToObjects11= [];
gdjs.TitleCode.GDHowToObjects12= [];
gdjs.TitleCode.GDHowToObjects13= [];
gdjs.TitleCode.GDHowToObjects14= [];
gdjs.TitleCode.GDHowToObjects15= [];
gdjs.TitleCode.GDHowToObjects16= [];
gdjs.TitleCode.GDHowToObjects17= [];
gdjs.TitleCode.GDHowToObjects18= [];
gdjs.TitleCode.GDHowToObjects19= [];
gdjs.TitleCode.GDHowToObjects20= [];
gdjs.TitleCode.GDHowToObjects21= [];


gdjs.TitleCode.asyncCallback9788836 = function (runtimeScene, asyncObjectsList) {
{runtimeScene.getScene().getVariables().get("Title").setNumber(1);
}}
gdjs.TitleCode.eventsList0 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.055), (runtimeScene) => (gdjs.TitleCode.asyncCallback9788836(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.TitleCode.asyncCallback9788612 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("title"), gdjs.TitleCode.GDtitleObjects20);

{for(var i = 0, len = gdjs.TitleCode.GDtitleObjects20.length ;i < len;++i) {
    gdjs.TitleCode.GDtitleObjects20[i].setAngle(0);
}
}
{ //Subevents
gdjs.TitleCode.eventsList0(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.TitleCode.eventsList1 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.TitleCode.GDtitleObjects19) asyncObjectsList.addObject("title", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.055), (runtimeScene) => (gdjs.TitleCode.asyncCallback9788612(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.TitleCode.asyncCallback9788388 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("title"), gdjs.TitleCode.GDtitleObjects19);

{for(var i = 0, len = gdjs.TitleCode.GDtitleObjects19.length ;i < len;++i) {
    gdjs.TitleCode.GDtitleObjects19[i].setAngle(-(5));
}
}
{ //Subevents
gdjs.TitleCode.eventsList1(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.TitleCode.eventsList2 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.TitleCode.GDtitleObjects18) asyncObjectsList.addObject("title", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.055), (runtimeScene) => (gdjs.TitleCode.asyncCallback9788388(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.TitleCode.asyncCallback9785500 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("title"), gdjs.TitleCode.GDtitleObjects18);

{for(var i = 0, len = gdjs.TitleCode.GDtitleObjects18.length ;i < len;++i) {
    gdjs.TitleCode.GDtitleObjects18[i].setAngle(-(10));
}
}
{ //Subevents
gdjs.TitleCode.eventsList2(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.TitleCode.eventsList3 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.TitleCode.GDtitleObjects17) asyncObjectsList.addObject("title", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.055), (runtimeScene) => (gdjs.TitleCode.asyncCallback9785500(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.TitleCode.asyncCallback9787532 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("title"), gdjs.TitleCode.GDtitleObjects17);

{for(var i = 0, len = gdjs.TitleCode.GDtitleObjects17.length ;i < len;++i) {
    gdjs.TitleCode.GDtitleObjects17[i].setAngle(-(15));
}
}
{ //Subevents
gdjs.TitleCode.eventsList3(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.TitleCode.eventsList4 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.TitleCode.GDtitleObjects16) asyncObjectsList.addObject("title", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.055), (runtimeScene) => (gdjs.TitleCode.asyncCallback9787532(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.TitleCode.asyncCallback9787260 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("title"), gdjs.TitleCode.GDtitleObjects16);

{for(var i = 0, len = gdjs.TitleCode.GDtitleObjects16.length ;i < len;++i) {
    gdjs.TitleCode.GDtitleObjects16[i].setAngle(-(20));
}
}
{ //Subevents
gdjs.TitleCode.eventsList4(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.TitleCode.eventsList5 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.TitleCode.GDtitleObjects15) asyncObjectsList.addObject("title", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.055), (runtimeScene) => (gdjs.TitleCode.asyncCallback9787260(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.TitleCode.asyncCallback9786988 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("title"), gdjs.TitleCode.GDtitleObjects15);

{for(var i = 0, len = gdjs.TitleCode.GDtitleObjects15.length ;i < len;++i) {
    gdjs.TitleCode.GDtitleObjects15[i].setAngle(-(25));
}
}
{ //Subevents
gdjs.TitleCode.eventsList5(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.TitleCode.eventsList6 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.TitleCode.GDtitleObjects14) asyncObjectsList.addObject("title", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.055), (runtimeScene) => (gdjs.TitleCode.asyncCallback9786988(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.TitleCode.asyncCallback9786716 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("title"), gdjs.TitleCode.GDtitleObjects14);

{for(var i = 0, len = gdjs.TitleCode.GDtitleObjects14.length ;i < len;++i) {
    gdjs.TitleCode.GDtitleObjects14[i].setAngle(-(20));
}
}
{ //Subevents
gdjs.TitleCode.eventsList6(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.TitleCode.eventsList7 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.TitleCode.GDtitleObjects13) asyncObjectsList.addObject("title", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.055), (runtimeScene) => (gdjs.TitleCode.asyncCallback9786716(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.TitleCode.asyncCallback9786444 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("title"), gdjs.TitleCode.GDtitleObjects13);

{for(var i = 0, len = gdjs.TitleCode.GDtitleObjects13.length ;i < len;++i) {
    gdjs.TitleCode.GDtitleObjects13[i].setAngle(-(15));
}
}
{ //Subevents
gdjs.TitleCode.eventsList7(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.TitleCode.eventsList8 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.TitleCode.GDtitleObjects12) asyncObjectsList.addObject("title", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.055), (runtimeScene) => (gdjs.TitleCode.asyncCallback9786444(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.TitleCode.asyncCallback9786172 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("title"), gdjs.TitleCode.GDtitleObjects12);

{for(var i = 0, len = gdjs.TitleCode.GDtitleObjects12.length ;i < len;++i) {
    gdjs.TitleCode.GDtitleObjects12[i].setAngle(-(10));
}
}
{ //Subevents
gdjs.TitleCode.eventsList8(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.TitleCode.eventsList9 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.TitleCode.GDtitleObjects11) asyncObjectsList.addObject("title", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.055), (runtimeScene) => (gdjs.TitleCode.asyncCallback9786172(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.TitleCode.asyncCallback9785900 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("title"), gdjs.TitleCode.GDtitleObjects11);

{for(var i = 0, len = gdjs.TitleCode.GDtitleObjects11.length ;i < len;++i) {
    gdjs.TitleCode.GDtitleObjects11[i].setAngle(-(5));
}
}
{ //Subevents
gdjs.TitleCode.eventsList9(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.TitleCode.eventsList10 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.TitleCode.GDtitleObjects10) asyncObjectsList.addObject("title", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.055), (runtimeScene) => (gdjs.TitleCode.asyncCallback9785900(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.TitleCode.asyncCallback9784396 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("title"), gdjs.TitleCode.GDtitleObjects10);

{for(var i = 0, len = gdjs.TitleCode.GDtitleObjects10.length ;i < len;++i) {
    gdjs.TitleCode.GDtitleObjects10[i].setAngle(0);
}
}
{ //Subevents
gdjs.TitleCode.eventsList10(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.TitleCode.eventsList11 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.TitleCode.GDtitleObjects9) asyncObjectsList.addObject("title", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.055), (runtimeScene) => (gdjs.TitleCode.asyncCallback9784396(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.TitleCode.asyncCallback9785228 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("title"), gdjs.TitleCode.GDtitleObjects9);

{for(var i = 0, len = gdjs.TitleCode.GDtitleObjects9.length ;i < len;++i) {
    gdjs.TitleCode.GDtitleObjects9[i].setAngle(5);
}
}
{ //Subevents
gdjs.TitleCode.eventsList11(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.TitleCode.eventsList12 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.TitleCode.GDtitleObjects8) asyncObjectsList.addObject("title", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.055), (runtimeScene) => (gdjs.TitleCode.asyncCallback9785228(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.TitleCode.asyncCallback9784956 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("title"), gdjs.TitleCode.GDtitleObjects8);

{for(var i = 0, len = gdjs.TitleCode.GDtitleObjects8.length ;i < len;++i) {
    gdjs.TitleCode.GDtitleObjects8[i].setAngle(10);
}
}
{ //Subevents
gdjs.TitleCode.eventsList12(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.TitleCode.eventsList13 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.TitleCode.GDtitleObjects7) asyncObjectsList.addObject("title", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.055), (runtimeScene) => (gdjs.TitleCode.asyncCallback9784956(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.TitleCode.asyncCallback9784684 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("title"), gdjs.TitleCode.GDtitleObjects7);

{for(var i = 0, len = gdjs.TitleCode.GDtitleObjects7.length ;i < len;++i) {
    gdjs.TitleCode.GDtitleObjects7[i].setAngle(15);
}
}
{ //Subevents
gdjs.TitleCode.eventsList13(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.TitleCode.eventsList14 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.TitleCode.GDtitleObjects6) asyncObjectsList.addObject("title", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.055), (runtimeScene) => (gdjs.TitleCode.asyncCallback9784684(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.TitleCode.asyncCallback9784532 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("title"), gdjs.TitleCode.GDtitleObjects6);

{for(var i = 0, len = gdjs.TitleCode.GDtitleObjects6.length ;i < len;++i) {
    gdjs.TitleCode.GDtitleObjects6[i].setAngle(20);
}
}
{ //Subevents
gdjs.TitleCode.eventsList14(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.TitleCode.eventsList15 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.TitleCode.GDtitleObjects5) asyncObjectsList.addObject("title", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.055), (runtimeScene) => (gdjs.TitleCode.asyncCallback9784532(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.TitleCode.asyncCallback9784124 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("title"), gdjs.TitleCode.GDtitleObjects5);

{for(var i = 0, len = gdjs.TitleCode.GDtitleObjects5.length ;i < len;++i) {
    gdjs.TitleCode.GDtitleObjects5[i].setAngle(25);
}
}
{ //Subevents
gdjs.TitleCode.eventsList15(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.TitleCode.eventsList16 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.TitleCode.GDtitleObjects4) asyncObjectsList.addObject("title", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.055), (runtimeScene) => (gdjs.TitleCode.asyncCallback9784124(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.TitleCode.asyncCallback9783852 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("title"), gdjs.TitleCode.GDtitleObjects4);

{for(var i = 0, len = gdjs.TitleCode.GDtitleObjects4.length ;i < len;++i) {
    gdjs.TitleCode.GDtitleObjects4[i].setAngle(20);
}
}
{ //Subevents
gdjs.TitleCode.eventsList16(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.TitleCode.eventsList17 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.TitleCode.GDtitleObjects3) asyncObjectsList.addObject("title", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.055), (runtimeScene) => (gdjs.TitleCode.asyncCallback9783852(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.TitleCode.asyncCallback9783508 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("title"), gdjs.TitleCode.GDtitleObjects3);

{for(var i = 0, len = gdjs.TitleCode.GDtitleObjects3.length ;i < len;++i) {
    gdjs.TitleCode.GDtitleObjects3[i].setAngle(15);
}
}
{ //Subevents
gdjs.TitleCode.eventsList17(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.TitleCode.eventsList18 = function(runtimeScene, asyncObjectsList) {

{


{
const parentAsyncObjectsList = asyncObjectsList;
{
const asyncObjectsList = gdjs.LongLivedObjectsList.from(parentAsyncObjectsList);
for (const obj of gdjs.TitleCode.GDtitleObjects2) asyncObjectsList.addObject("title", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.055), (runtimeScene) => (gdjs.TitleCode.asyncCallback9783508(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.TitleCode.asyncCallback9783292 = function (runtimeScene, asyncObjectsList) {
gdjs.copyArray(asyncObjectsList.getObjects("title"), gdjs.TitleCode.GDtitleObjects2);

{for(var i = 0, len = gdjs.TitleCode.GDtitleObjects2.length ;i < len;++i) {
    gdjs.TitleCode.GDtitleObjects2[i].setAngle(10);
}
}
{ //Subevents
gdjs.TitleCode.eventsList18(runtimeScene, asyncObjectsList);} //End of subevents
}
gdjs.TitleCode.eventsList19 = function(runtimeScene) {

{


{
{
const asyncObjectsList = new gdjs.LongLivedObjectsList();
for (const obj of gdjs.TitleCode.GDtitleObjects1) asyncObjectsList.addObject("title", obj);
runtimeScene.getAsyncTasksManager().addTask(gdjs.evtTools.runtimeScene.wait(0.055), (runtimeScene) => (gdjs.TitleCode.asyncCallback9783292(runtimeScene, asyncObjectsList)));
}
}

}


};gdjs.TitleCode.eventsList20 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("BlackSquareDecoratedButton"), gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects1.length;i<l;++i) {
    if ( gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects1[i].IsClicked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects1[k] = gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects1[i];
        ++k;
    }
}
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects1.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "GamePlay", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("HowTo"), gdjs.TitleCode.GDHowToObjects1);
gdjs.copyArray(runtimeScene.getObjects("title"), gdjs.TitleCode.GDtitleObjects1);
{for(var i = 0, len = gdjs.TitleCode.GDtitleObjects1.length ;i < len;++i) {
    gdjs.TitleCode.GDtitleObjects1[i].rotateTowardAngle(-(35), 10, runtimeScene);
}
}{runtimeScene.getScene().getVariables().get("Title").setNumber(1);
}{gdjs.evtTools.sound.playMusic(runtimeScene, "5325981204873216.wav", true, 100, 1);
}{for(var i = 0, len = gdjs.TitleCode.GDHowToObjects1.length ;i < len;++i) {
    gdjs.TitleCode.GDHowToObjects1[i].hide();
}
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().get("Title")) == 1;
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(9782716);
}
}
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("title"), gdjs.TitleCode.GDtitleObjects1);
{runtimeScene.getScene().getVariables().get("Title").setNumber(0);
}{for(var i = 0, len = gdjs.TitleCode.GDtitleObjects1.length ;i < len;++i) {
    gdjs.TitleCode.GDtitleObjects1[i].setAngle(5);
}
}
{ //Subevents
gdjs.TitleCode.eventsList19(runtimeScene);} //End of subevents
}

}


{

gdjs.copyArray(runtimeScene.getObjects("help"), gdjs.TitleCode.GDhelpObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.TitleCode.GDhelpObjects1.length;i<l;++i) {
    if ( gdjs.TitleCode.GDhelpObjects1[i].IsClicked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.TitleCode.GDhelpObjects1[k] = gdjs.TitleCode.GDhelpObjects1[i];
        ++k;
    }
}
gdjs.TitleCode.GDhelpObjects1.length = k;
if (isConditionTrue_0) {
gdjs.copyArray(runtimeScene.getObjects("HowTo"), gdjs.TitleCode.GDHowToObjects1);
{for(var i = 0, len = gdjs.TitleCode.GDHowToObjects1.length ;i < len;++i) {
    gdjs.TitleCode.GDHowToObjects1[i].hide(false);
}
}}

}


};

gdjs.TitleCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects1.length = 0;
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects2.length = 0;
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects3.length = 0;
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects4.length = 0;
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects5.length = 0;
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects6.length = 0;
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects7.length = 0;
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects8.length = 0;
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects9.length = 0;
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects10.length = 0;
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects11.length = 0;
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects12.length = 0;
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects13.length = 0;
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects14.length = 0;
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects15.length = 0;
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects16.length = 0;
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects17.length = 0;
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects18.length = 0;
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects19.length = 0;
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects20.length = 0;
gdjs.TitleCode.GDBlackSquareDecoratedButtonObjects21.length = 0;
gdjs.TitleCode.GDhelpObjects1.length = 0;
gdjs.TitleCode.GDhelpObjects2.length = 0;
gdjs.TitleCode.GDhelpObjects3.length = 0;
gdjs.TitleCode.GDhelpObjects4.length = 0;
gdjs.TitleCode.GDhelpObjects5.length = 0;
gdjs.TitleCode.GDhelpObjects6.length = 0;
gdjs.TitleCode.GDhelpObjects7.length = 0;
gdjs.TitleCode.GDhelpObjects8.length = 0;
gdjs.TitleCode.GDhelpObjects9.length = 0;
gdjs.TitleCode.GDhelpObjects10.length = 0;
gdjs.TitleCode.GDhelpObjects11.length = 0;
gdjs.TitleCode.GDhelpObjects12.length = 0;
gdjs.TitleCode.GDhelpObjects13.length = 0;
gdjs.TitleCode.GDhelpObjects14.length = 0;
gdjs.TitleCode.GDhelpObjects15.length = 0;
gdjs.TitleCode.GDhelpObjects16.length = 0;
gdjs.TitleCode.GDhelpObjects17.length = 0;
gdjs.TitleCode.GDhelpObjects18.length = 0;
gdjs.TitleCode.GDhelpObjects19.length = 0;
gdjs.TitleCode.GDhelpObjects20.length = 0;
gdjs.TitleCode.GDhelpObjects21.length = 0;
gdjs.TitleCode.GDtitleObjects1.length = 0;
gdjs.TitleCode.GDtitleObjects2.length = 0;
gdjs.TitleCode.GDtitleObjects3.length = 0;
gdjs.TitleCode.GDtitleObjects4.length = 0;
gdjs.TitleCode.GDtitleObjects5.length = 0;
gdjs.TitleCode.GDtitleObjects6.length = 0;
gdjs.TitleCode.GDtitleObjects7.length = 0;
gdjs.TitleCode.GDtitleObjects8.length = 0;
gdjs.TitleCode.GDtitleObjects9.length = 0;
gdjs.TitleCode.GDtitleObjects10.length = 0;
gdjs.TitleCode.GDtitleObjects11.length = 0;
gdjs.TitleCode.GDtitleObjects12.length = 0;
gdjs.TitleCode.GDtitleObjects13.length = 0;
gdjs.TitleCode.GDtitleObjects14.length = 0;
gdjs.TitleCode.GDtitleObjects15.length = 0;
gdjs.TitleCode.GDtitleObjects16.length = 0;
gdjs.TitleCode.GDtitleObjects17.length = 0;
gdjs.TitleCode.GDtitleObjects18.length = 0;
gdjs.TitleCode.GDtitleObjects19.length = 0;
gdjs.TitleCode.GDtitleObjects20.length = 0;
gdjs.TitleCode.GDtitleObjects21.length = 0;
gdjs.TitleCode.GDHowToObjects1.length = 0;
gdjs.TitleCode.GDHowToObjects2.length = 0;
gdjs.TitleCode.GDHowToObjects3.length = 0;
gdjs.TitleCode.GDHowToObjects4.length = 0;
gdjs.TitleCode.GDHowToObjects5.length = 0;
gdjs.TitleCode.GDHowToObjects6.length = 0;
gdjs.TitleCode.GDHowToObjects7.length = 0;
gdjs.TitleCode.GDHowToObjects8.length = 0;
gdjs.TitleCode.GDHowToObjects9.length = 0;
gdjs.TitleCode.GDHowToObjects10.length = 0;
gdjs.TitleCode.GDHowToObjects11.length = 0;
gdjs.TitleCode.GDHowToObjects12.length = 0;
gdjs.TitleCode.GDHowToObjects13.length = 0;
gdjs.TitleCode.GDHowToObjects14.length = 0;
gdjs.TitleCode.GDHowToObjects15.length = 0;
gdjs.TitleCode.GDHowToObjects16.length = 0;
gdjs.TitleCode.GDHowToObjects17.length = 0;
gdjs.TitleCode.GDHowToObjects18.length = 0;
gdjs.TitleCode.GDHowToObjects19.length = 0;
gdjs.TitleCode.GDHowToObjects20.length = 0;
gdjs.TitleCode.GDHowToObjects21.length = 0;

gdjs.TitleCode.eventsList20(runtimeScene);

return;

}

gdjs['TitleCode'] = gdjs.TitleCode;
