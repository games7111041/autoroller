gdjs.GameOverCode = {};
gdjs.GameOverCode.GDGameOverTXTObjects1= [];
gdjs.GameOverCode.GDGameOverTXTObjects2= [];
gdjs.GameOverCode.GDBlackSquareDecoratedButtonObjects1= [];
gdjs.GameOverCode.GDBlackSquareDecoratedButtonObjects2= [];


gdjs.GameOverCode.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("BlackSquareDecoratedButton"), gdjs.GameOverCode.GDBlackSquareDecoratedButtonObjects1);

let isConditionTrue_0 = false;
isConditionTrue_0 = false;
for (var i = 0, k = 0, l = gdjs.GameOverCode.GDBlackSquareDecoratedButtonObjects1.length;i<l;++i) {
    if ( gdjs.GameOverCode.GDBlackSquareDecoratedButtonObjects1[i].IsClicked((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined)) ) {
        isConditionTrue_0 = true;
        gdjs.GameOverCode.GDBlackSquareDecoratedButtonObjects1[k] = gdjs.GameOverCode.GDBlackSquareDecoratedButtonObjects1[i];
        ++k;
    }
}
gdjs.GameOverCode.GDBlackSquareDecoratedButtonObjects1.length = k;
if (isConditionTrue_0) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Title", false);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().get("L")) == 1;
if (isConditionTrue_0) {
{gdjs.evtTools.leaderboards.displayLeaderboard(runtimeScene, "40ffb5b2-0a88-4a4d-a494-74e722f50eee", true);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().get("L")) == 0;
if (isConditionTrue_0) {
{gdjs.evtTools.leaderboards.closeLeaderboardView(runtimeScene);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.input.isKeyPressed(runtimeScene, "l");
if (isConditionTrue_0) {
isConditionTrue_0 = false;
{isConditionTrue_0 = runtimeScene.getOnceTriggers().triggerOnce(9859900);
}
}
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().get("L").add(1);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getScene().getVariables().get("L")) > 1;
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().get("L").setNumber(0);
}}

}


{


let isConditionTrue_0 = false;
isConditionTrue_0 = false;
isConditionTrue_0 = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
if (isConditionTrue_0) {
{runtimeScene.getScene().getVariables().get("L").setNumber(0);
}}

}


};

gdjs.GameOverCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.GameOverCode.GDGameOverTXTObjects1.length = 0;
gdjs.GameOverCode.GDGameOverTXTObjects2.length = 0;
gdjs.GameOverCode.GDBlackSquareDecoratedButtonObjects1.length = 0;
gdjs.GameOverCode.GDBlackSquareDecoratedButtonObjects2.length = 0;

gdjs.GameOverCode.eventsList0(runtimeScene);

return;

}

gdjs['GameOverCode'] = gdjs.GameOverCode;
